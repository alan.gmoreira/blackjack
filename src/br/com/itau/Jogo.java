package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogo {
    private Baralho baralho;
    private Jogador jogador;
    private String logJogo;
    private int pontuacao;

    public Jogo(Jogador jogador) {
        this.baralho = new Baralho();
        this.baralho.embaralhar();
        this.jogador = jogador;
    }

    public Jogo() {
    }

    public Baralho getBaralho() {
        return baralho;
    }

    public void setBaralho(Baralho baralho) {
        this.baralho = baralho;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public String getLogJogo() {
        return logJogo;
    }

    public void setLogJogo(String logJogo) {
        this.logJogo = logJogo;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }

    public List<Carta> distribuiMaoInicial() {
        List<Carta> maoInicial = new ArrayList<Carta>();
        Carta carta1 = baralho.puxarCarta();
        Carta carta2 = baralho.puxarCarta();

        maoInicial.add(carta1);
        maoInicial.add(carta2);

        this.logJogo += "Mão inicial: " + carta1.toString() + ", " + carta2.toString() + " | ";

        this.pontuacao += carta1.getValorCarta().valor + carta2.getValorCarta().valor;

        return maoInicial;
    }

    public Carta puxarCarta(){
        Carta carta = baralho.puxarCarta();

        this.logJogo += "Carta puxada: " + carta.toString() + " | ";

        this.pontuacao += carta.getValorCarta().valor;

        return carta;
    }

    public void finalizarJogo(){
        if(this.pontuacao <= 21)
            this.jogador.addJogo(this);
    }
}
