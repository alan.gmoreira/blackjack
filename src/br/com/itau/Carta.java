package br.com.itau;

public class Carta {
    private ValorCarta valorCarta;
    private Naipe naipe;

    public Carta(ValorCarta valorCarta, Naipe naipe) {
        this.valorCarta = valorCarta;
        this.naipe = naipe;
    }

    public ValorCarta getValorCarta() {
        return valorCarta;
    }

    public void setValorCarta(ValorCarta valorCarta) {
        this.valorCarta = valorCarta;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }

    @Override
    public String toString() {
        return this.valorCarta + " de " + this.naipe;
    }
}
