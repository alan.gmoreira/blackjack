package br.com.itau;

import java.util.List;
import java.util.Scanner;

public class IO {

    private static Scanner scan = new Scanner(System.in);

    public static void iniciarJogo() throws Exception {
        Jogador jogador;
        Jogo jogo;
        List<Carta> cartasJogadas;
        String saidaJogo = "";
        String paradaJogo = "";

        System.out.println("BlackJack Iniciado.");

        System.out.println("Qual seu nome?");
        jogador = new Jogador(scan.next());

        while (!saidaJogo.equalsIgnoreCase("sair")) {
            jogo = new Jogo(jogador);
            System.out.println(jogador.getNome() + "Jogo iniciado.");

            cartasJogadas = jogo.distribuiMaoInicial();

            if (cartasJogadas.size() != 2)
                throw new Exception("Houve algum problema na distribuição de cartas.");

            System.out.println("Mão inicial: " + cartasJogadas.get(0).toString() + ", "
                    + cartasJogadas.get(1).toString() +
                    " | Pontuação atual: " + jogo.getPontuacao());

            System.out.println("Digite 's' para puxar outra carta ou qualquer outra coisa para parar!");
            paradaJogo = scan.next();

            while (paradaJogo.equalsIgnoreCase("s")) {
                System.out.println("Carta puxada: " + jogo.puxarCarta().toString() + " | Pontuação atual: " + jogo.getPontuacao());
                if (jogo.getPontuacao() > 21) {
                    System.out.println("Voce perdeu!");
                    paradaJogo = "sair";
                } else {
                    System.out.println("Puxar carta(s)?");
                    paradaJogo = scan.next();
                }

            }
            jogo.finalizarJogo();

        }
    }
}
