package br.com.itau;

public enum ValorCarta {
    A(1),J(10),Q(10),K(10), DOIS(2), TRES(3), QUATRO(4), CINCO(5), SEIS(6), SETE(7), OITO(8), NOVE(9), DEZ(10);

    public int valor;
    ValorCarta(int i) {
        valor = i;
    }
}
