package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Jogador {
    private String Nome;
    private List<Jogo> jogos;

    public Jogador(String nome, List<Jogo> jogos) {
        Nome = nome;
        this.jogos = jogos;
    }

    public Jogador(String nome) {
        Nome = nome;
        this.jogos = new ArrayList<Jogo>();
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public List<Jogo> getJogos() {
        return jogos;
    }

    public void setJogos(List<Jogo> jogos) {
        this.jogos = jogos;
    }

    public void addJogo(Jogo jogo) {
        this.jogos.add(jogo);
    }
}
