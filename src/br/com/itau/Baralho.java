package br.com.itau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Baralho {
    private List<Carta> cartas;

    public Baralho(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public Baralho() {
        cartas = new ArrayList<Carta>();

        for (ValorCarta valorCarta : ValorCarta.values()
        ) {
            for (Naipe naipe : Naipe.values()
                 ){
                    cartas.add(new Carta(valorCarta, naipe));
            }
        }

    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(List<Carta> cartas) {
        this.cartas = cartas;
    }

    public void embaralhar() {
        Collections.shuffle(cartas);
    }

    public Carta puxarCarta() {
        Carta retornoCarta;

        retornoCarta = cartas.size() > 0 ? cartas.get(0) : null;

        cartas.remove(0);

        return retornoCarta;
    }

}
